(ns jobtechdev.esco-skill
  (:require
   [clj-http.client :as client]
   [cheshire.core :refer :all]
   [wanderung.core :as w]
   [mount.core :refer [defstate]]
   [datahike.api :as d]
   [clojure.pprint :as pp]
   [taoensso.nippy :as nippy]
   [cheshire.core :as json])
  (:gen-class))


(def mem-cfg
  {:wanderung/type :datahike
   :store {:backend :mem
           :id "taxonomy"}
   :schema-flexibility :write
   :attribute-refs? true
   :index :datahike.index/persistent-set
   })

(def nippy-cfg
  {:wanderung/type :nippy
   :filename "resources/taxonomy-v21.nippy"})


(defn load-database []
  (w/migrate nippy-cfg mem-cfg))


(defn create-conn []
  (do
    (load-database)
    (d/connect mem-cfg)))

(defstate  ^{:on-reload :noop}
  conn :start (create-conn))

(def get-skill-exact-match-esco-skill-query
  '[:find ?c1id ?esco-id  ;; ?c1id ?c1p ?c2id ?c2p
    :in $
    :where
    [?r :relation/type "exact-match"]
    [?r :relation/concept-1 ?c1]
    [?c1 :concept/type "skill"]
    [?c1 :concept/id ?c1id]
    ;; [?c1 :concept/preferred-label ?c1p]
    [?r :relation/concept-2 ?c2]
    [?c2 :concept/type "esco-skill"]
    [?c2 :concept/id ?c2id]
    [?c2 :concept.external-standard/esco-uri ?esco-id]
    ;; [?c2 :concept/preferred-label ?c2p]
    ])


(defn get-skill-exact-match-esco-skill []
  (d/q get-skill-exact-match-esco-skill-query
       @conn))

(defn get-skill-lookup []
  (into
   {}
   (get-skill-exact-match-esco-skill)))


(defn write-skill-lookup-to-file []
  (spit "resources/esco-skill-lookup.json"
        (generate-string
         (get-skill-lookup)
         {:pretty true}
         )))
