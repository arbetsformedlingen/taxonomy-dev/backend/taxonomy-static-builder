(ns jobtechdev.isced
  (:require
   [clj-http.client :as client]
   [cheshire.core :refer :all]
   [dk.ative.docjure.spreadsheet :as dk]
   [cheshire.core :as json])
  (:gen-class))

;; sun-education-level-2
;; sun-education-field-3

(def root-folder "resources/isced/")

(defn load-sun-2000-education-field-3->isced []
  (->> (dk/load-workbook "resources/isced/sun-2000-isced-2013-f.xlsx")
       (dk/select-sheet "SUN2000Inr_3_ISCED_F_2013")
       (dk/select-columns {:A :sun-code, :B :sun-label :C :isced-code :D :isced-label})
       (filter :sun-code)
       rest
       ))

(defn load-sun-2000-education-field-4->isced []
  (->> (dk/load-workbook "resources/isced/sun-2000-isced-2013-f.xlsx")
       (dk/select-sheet "SUN2000Inr_ISCED_F_2013")
       (dk/select-columns {:A :sun-code, :B :sun-label :C :isced-code :D :isced-label})
       (filter :sun-code)
       rest
       ))

(defn load-sun-2020-education-field-2->isced []
  (->> (dk/load-workbook "resources/isced/sun-2020-isced-2013-f.xlsx")
       (dk/select-sheet "SUN2020Inr_2_ISCED F_2013_2 ")
       (dk/select-columns {:A :sun-code, :B :sun-label :C :isced-code :D :isced-label})
       (filter :sun-code)
       rest
       ))

(defn load-sun-2020-education-field-3->isced []
  (->> (dk/load-workbook "resources/isced/sun-2020-isced-2013-f.xlsx")
       (dk/select-sheet "SUN2020Inr_3_ISCED F_2013_4")
       (dk/select-columns {:A :sun-code, :B :sun-label :C :isced-code :D :isced-label})
       (filter :sun-code)
       rest
       ))

(defn load-sun-2020-education-field-4->isced []
  (->> (dk/load-workbook "resources/isced/sun-2020-isced-2013-f.xlsx")
       (dk/select-sheet "SUN2020Inr_4_ISCED F_2013_4")
       (dk/select-columns {:A :sun-code, :B :sun-label :C :isced-code :D :isced-label})
       (filter :sun-code)
       rest
       ))

;; Obs den get isced 97 koder för sun 2000
(defn load-sun-2000-education-level-3->isced []
  (->> (dk/load-workbook (str root-folder  "nyckel-sun-2020_isced2011_isced97.xlsx"))
       (dk/select-sheet "Nyckel_SUN2000Niva_ISCED97")
       (dk/select-columns {:A :sun-code, :B :sun-label :C :isced-code :D :isced-label})
       (filter :sun-code)
       rest
       ))

(defn load-sun-2020-education-level-3->isced []
  (->> (dk/load-workbook (str root-folder "nyckel-sun-2020_isced2011_isced97.xlsx"))
       (dk/select-sheet "Nyckel_SUN2020Niva_ISCED2011_1")
       (dk/select-columns {:A :sun-code, :B :sun-label :C :isced-code :D :isced-label})
       (filter :sun-code)
       rest))

(defn sun->isced [sun-code isced-list]
  (first (map :isced-code (filter #(= (:sun-code %) sun-code)  isced-list))))

(def sun-education-query
  "query MyQuery {
  concepts(type: [\"sun-education-field-1\"
   	          \"sun-education-field-2\"
                  \"sun-education-field-3\"
                  \"sun-education-field-4\"
                  \"sun-education-level-1\"
                  \"sun-education-level-2\"
                  \"sun-education-level-3\"
                  \"sun-education-level-4\"], include_deprecated: true) {
    id
    preferred_label
    type
    sun_education_level_code_2020
    sun_education_level_code_2000
    sun_education_field_code_2000
    sun_education_field_code_2020
}}")


(defn load-sun-education []
  (get-in (client/get "https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphql" {:accept :json :as :json :query-params {"query" sun-education-query}}) [:body :data :concepts]))


(defn chop-last-character [s]
  (apply str (drop-last s)))

(defn build-generalized-sun-isced-code-lookup [sun-isced-list]
  (reduce
   (fn [acc element]
     (assoc acc
            (chop-last-character (:sun-code element))
            (chop-last-character (:isced-code element))))
   {}
   sun-isced-list))

(defn build-generalized-sun-isced-code-lookup-from-lookup [sun-isced-lookup]
  (into {} (map
            (fn [[sun isced]]
                   {(chop-last-character sun)
                    (chop-last-character isced)})
              sun-isced-lookup)))


(defn sun-2000-education-field-2->isced-lookup []
  (build-generalized-sun-isced-code-lookup (load-sun-2000-education-field-3->isced)))

(defn sun-2000-education-field-1->isced-lookup []
  (build-generalized-sun-isced-code-lookup-from-lookup (sun-2000-education-field-2->isced-lookup)))

(defn sun-2020-education-field-2->isced-lookup []
  (build-generalized-sun-isced-code-lookup (load-sun-2020-education-field-3->isced)))

(defn sun-2020-education-field-1->isced-lookup []
  (build-generalized-sun-isced-code-lookup-from-lookup (sun-2020-education-field-2->isced-lookup)))

(defn sun-2000-education-level-2->isced-lookup []
  (reduce
   (fn [acc element]
     (assoc acc
            (chop-last-character (:sun-code element))
            (if (< 1 (count (:isced-code element)))
              (chop-last-character (:isced-code element))
              (:isced-code element))))
   {}
   (load-sun-2000-education-level-3->isced))
  )

;; https://www.scb.se/contentassets/aeeedec0e28c465aa524429407dcd5ba/implementering-av-isced-2011.pdf

(defn sun-2000-education-level-1->isced-lookup []
  {
   "0" "0"
   "1" "1"
   "2" "2"
   "3" "3"
   "4" "4"
   "5" "5"
   "6" "8"}
  )

(defn sun-2020-education-level-2->isced-lookup []
  (reduce
   (fn [acc element]
     (assoc acc
            (chop-last-character (:sun-code element))
            (if (< 1 (count (:isced-code element)))
              (chop-last-character (:isced-code element))
              (:isced-code element))))
   {}
   (load-sun-2020-education-level-3->isced)))


(defn sun-2020-education-level-1->isced-lookup []
  {
   "0" "0"
   "1" "1"
   "2" "2"
   "3" "3"
   "4" "4"
   "5" "5"
   "6" "8"}
  )


(defn load-sun-2000-education-level-1 []
  (filter #(and (:sun_education_level_code_2000 %)
                (= "sun-education-level-1" (:type %))) (load-sun-education)))

(defn load-sun-2020-education-level-1 []
  (filter #(and (:sun_education_level_code_2020 %)
                (= "sun-education-level-1" (:type %))) (load-sun-education)))


(defn load-sun-2000-education-level-2 []
  (filter #(and (:sun_education_level_code_2000 %)
                (= "sun-education-level-2" (:type %))) (load-sun-education)))


(defn load-sun-2020-education-level-2 []
  (filter #(and (:sun_education_level_code_2020 %)
                (= "sun-education-level-2" (:type %))) (load-sun-education)))

(defn load-sun-2000-education-field-1 []
  (filter #(and (:sun_education_field_code_2000 %)
                (= "sun-education-field-1" (:type %))) (load-sun-education)))

(defn load-sun-2020-education-field-1 []
  (filter #(and (:sun_education_field_code_2020 %)
                (= "sun-education-field-1" (:type %))) (load-sun-education)))

(defn load-sun-2000-education-field-2 []
  (filter #(and (:sun_education_field_code_2000 %)
                (= "sun-education-field-2" (:type %))) (load-sun-education)))

(defn load-sun-2020-education-field-2 []
  (filter #(and (:sun_education_field_code_2020 %)
                (= "sun-education-field-2" (:type %))) (load-sun-education)))



(defn load-sun-2000-education-level-3 []
  (filter #(and (:sun_education_level_code_2000 %)
                (= "sun-education-level-3" (:type %))) (load-sun-education)))

(defn load-sun-2020-education-level-3 []
  (filter #(and (:sun_education_level_code_2020 %)
                (= "sun-education-level-3" (:type %))) (load-sun-education)))

(defn load-sun-2000-education-field-3 []
  (filter #(and (:sun_education_field_code_2000 %)
                (= "sun-education-field-3" (:type %))) (load-sun-education)))

(defn load-sun-2020-education-field-3 []
  (filter #(and (:sun_education_field_code_2020 %)
                (= "sun-education-field-3" (:type %))) (load-sun-education)))

(defn load-sun-2020-education-field-4 []
  (filter #(and (:sun_education_field_code_2020 %)
                (= "sun-education-field-4" (:type %))) (load-sun-education)))

(defn build-sun-2000-education-level-1-old []
  (reduce
   (fn [acc e]
     (assoc acc (:id e)
            (get (sun-2000-education-level-1->isced-lookup)
                 (:sun_education_level_code_2000 e))))
   {}
   (load-sun-2000-education-level-1)))

(defn create-lookup-level-1-2 [sun-key sun-isced-lookup sun-data]
  (reduce
   (fn [acc e]
     (assoc acc (:id e)
            (get (sun-isced-lookup) (sun-key e))))
   {}
   sun-data))


(defn build-sun-2000-education-level-1 []
  (create-lookup-level-1-2
   :sun_education_level_code_2000
   sun-2000-education-level-1->isced-lookup
   (load-sun-2000-education-level-1)))

(defn build-sun-2020-education-level-1 []
  (create-lookup-level-1-2
   :sun_education_level_code_2020
   sun-2020-education-level-1->isced-lookup
   (load-sun-2020-education-level-1)))

(defn build-sun-2000-education-level-2 []
  (create-lookup-level-1-2
   :sun_education_level_code_2000
   sun-2000-education-level-2->isced-lookup
   (load-sun-2000-education-level-2)))

(defn build-sun-2020-education-level-2 []
  (create-lookup-level-1-2
   :sun_education_level_code_2020
   sun-2020-education-level-2->isced-lookup
   (load-sun-2020-education-level-2)))


(defn build-sun-2000-education-field-1 []
  (create-lookup-level-1-2
   :sun_education_field_code_2000
   sun-2000-education-field-1->isced-lookup
   (load-sun-2000-education-field-1)))

(defn build-sun-2020-education-field-1 []
  (create-lookup-level-1-2
   :sun_education_field_code_2020
   sun-2020-education-field-1->isced-lookup
   (load-sun-2020-education-field-1)))

(defn build-sun-2000-education-field-2 []
  (create-lookup-level-1-2
   :sun_education_field_code_2000
   sun-2000-education-field-2->isced-lookup
   (load-sun-2000-education-field-2)))

#_(defn build-sun-2020-education-field-2 []
  (create-lookup-level-1-2
   :sun_education_field_code_2020
   sun-2020-education-field-2->isced-lookup
   (load-sun-2020-education-field-2)))



(defn lookup-reduce-fun [sun-key sun->isced-data]
  (fn [acc element]
    (assoc
     acc
     (:id element)
     (sun->isced (sun-key element) sun->isced-data))))

(defn build-sun-2000-education-level-3 []
  (reduce
   (lookup-reduce-fun
    :sun_education_level_code_2000
    (load-sun-2000-education-level-3->isced))
   {}
   (load-sun-2000-education-level-3)))

(defn build-sun-2020-education-level-3 []
  (reduce
   (lookup-reduce-fun
    :sun_education_level_code_2020
    (load-sun-2020-education-level-3->isced))
   {}
   (load-sun-2020-education-level-3)))

(defn build-sun-2000-education-field-3 []
  (reduce
   (lookup-reduce-fun
    :sun_education_field_code_2000
    (load-sun-2000-education-field-3->isced))
   {}
   (load-sun-2000-education-field-3)))

(defn build-sun-2020-education-field-2 []
  (reduce
   (lookup-reduce-fun
    :sun_education_field_code_2020
    (load-sun-2020-education-field-2->isced))
   {}
   (load-sun-2020-education-field-2)))


(defn build-sun-2020-education-field-3 []
  (reduce
   (lookup-reduce-fun
    :sun_education_field_code_2020
    (load-sun-2020-education-field-3->isced))
   {}
   (load-sun-2020-education-field-3)))

;; We dont have sun education 2000 field 4

(defn build-sun-2020-education-field-4 []
  (reduce
   (lookup-reduce-fun
    :sun_education_field_code_2020
    (load-sun-2020-education-field-4->isced))
   {}
   (load-sun-2020-education-field-4)))

(defn build-lookup-sun-isced []
  (merge
   (build-sun-2000-education-level-1)
   (build-sun-2020-education-level-1)

   (build-sun-2000-education-level-2)
   (build-sun-2020-education-level-2)

   (build-sun-2000-education-level-3)
   (build-sun-2020-education-level-3)


   (build-sun-2000-education-field-1)
   (build-sun-2020-education-field-1)

   (build-sun-2000-education-field-2)
   (build-sun-2020-education-field-2)

   (build-sun-2000-education-field-3)
   (build-sun-2020-education-field-3)

   ;; (build-sun-2000-education-field-4)  we dont have sun 2000 education field 4

   (build-sun-2020-education-field-4)))


(defn write-isced-lookup-to-file []
  (spit "jobtech-taxonomy-to-isced.json" (generate-string (build-lookup-sun-isced))))
