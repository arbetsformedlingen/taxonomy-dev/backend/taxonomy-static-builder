(ns jobtechdev.taxonomy-static-builder
  (:require
   [clj-http.client :as client]
   [cheshire.core :refer :all]
   )
  (:gen-class))

(defn greet
  "Callable entry point to the application."
  [data]
  (println (str "Hello, " (or (:name data) "World") "!")))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (greet {:name (first args)}))




(defn esco-occupations-version-query [version]
  (format "query MyQuery {
  concepts(type: \"occupation-name\", version: \"%s\"  ) {
    id
    preferred_label
    type
    exact_match{
      id
      esco_uri
      preferred_label
      type
    }
    broad_match {
      id
      esco_uri
      preferred_label
      type
    }
    close_match {
      id
      esco_uri
      preferred_label
      type
    }
    narrow_match{
      id
      esco_uri
      preferred_label
      type
    }
    broader(type: \"isco-level-4\"){
      id
      preferred_label
      type
      esco_uri
    }
  }
}" version))

(def esco-occupations-query
  "query MyQuery {
  concepts(type: \"occupation-name\", include_deprecated: true ) {
    id
    preferred_label
    type
    exact_match{
      id
      esco_uri
      preferred_label
      type
    }
    broad_match {
      id
      esco_uri
      preferred_label
      type
    }
    close_match {
      id
      esco_uri
      preferred_label
      type
    }
    narrow_match{
      id
      esco_uri
      preferred_label
      type
    }
    broader(type: \"isco-level-4\"){
      id
      preferred_label
      type
      esco_uri
    }
  }
}")

#_(defn get-esco-occupations []
  #_(graphql/execute esco-occupations-query
                   nil
                   nil
                   ""))

(defn get-esco-occupations []
   (:body (client/get "https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphql" {:accept :json :as :json :query-params {"query" esco-occupations-query}}))
  )


(defn parse-esco-occupations-concepts [response]
  (get-in response [:data :concepts])
  )


(defn convert-concept-to-esco-ids [concept-with-relations]
  (let [concept concept-with-relations
        broad-match   (:broad_match concept)
        close-match   (:close_match concept)
        exact-match   (:exact_match concept)
        narrow-match  (:narrow_match concept)

        broad-match-count  (count broad-match)
        close-match-count  (count close-match)
        exact-match-count  (count exact-match)
        narrow-match-count (count narrow-match)

        total-match-count (+ broad-match-count close-match-count exact-match-count narrow-match-count)
        taxonomy-concept (:id concept)

        eures-concepts (if (< 1 total-match-count)
                         (if (#{1 2 3} exact-match-count)
                           exact-match
                           (if (< 3 total-match-count)
                             (:broader concept)
                             (if (not-empty narrow-match)
                               (:broader concept)
                               (concat broad-match close-match exact-match narrow-match))))
                         (concat broad-match close-match exact-match narrow-match))
        ]
    (if (empty? eures-concepts)
        [taxonomy-concept (map :esco_uri (:broader concept-with-relations))]
        [taxonomy-concept (map :esco_uri eures-concepts)]
        )
    ))


(defn apply-logic-from-taxonomy-to-esco [all-concepts]
  (reduce (fn [acc el]
            (let [[concept-id esco-ids]  (convert-concept-to-esco-ids el)]
              (assoc acc concept-id esco-ids)
              )
            ) {} all-concepts)
  )

(defn taxonomy->eures-map []
  (apply-logic-from-taxonomy-to-esco (parse-esco-occupations-concepts (get-esco-occupations)))
  )

(def taxonomy->eures-map-memoized (memoize taxonomy->eures-map))

(defn lookup-occupation-name-id-to-esco-ids [concept-id]
  (get (taxonomy->eures-map-memoized) concept-id)
  )


;; (client/get "https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphql" {:accept :json :as :json :query-params {"query" esco-occupations-query}})

(defn write-eures-map-to-file []
  (spit "jobtech-taxonomy-to-eures-esco.json" (generate-string (taxonomy->eures-map)))
  )
