(ns jobtechdev.taxonomy-static-legacy-converter
  (:require
   [wanderung.core :as w]
   [mount.core :refer [defstate]]
   [datahike.api :as d]
   [clojure.pprint :as pp]
   [taoensso.nippy :as nippy]
   )
  (:gen-class))

(def mem-cfg
  {:wanderung/type :datahike
   :store {:backend :mem
           :id "taxonomy"}
   :schema-flexibility :write
   :attribute-refs? true
   :index :datahike.index/persistent-set
   })

(def nippy-cfg
  {:wanderung/type :nippy
   :filename "resources/taxonomy-v21.nippy"})



(defn load-database []
  (w/migrate nippy-cfg mem-cfg))


(defn create-conn []
  (do
    (load-database)
    (d/connect mem-cfg)))

(defstate  ^{:on-reload :noop}
  conn :start (create-conn))

(def get-legacy-concepts-query
  '[:find (pull ?c pull-pattern)
    :in $ pull-pattern
    :where
    [?c :concept.external-database.ams-taxonomy-67/id]])


#_[:concept.external-standard/ssyk-code-2012
 :concept.external-standard/lau-2-code-2015
 :concept.external-standard/national-nuts-level-3-code-2019]

(def concept-pull-pattern [:concept/id
                           :concept/type
                           :concept/preferred-label
                           :concept/definition
                           :concept.external-database.ams-taxonomy-67/id])


(defn get-legacy-concepts []
  (d/q get-legacy-concepts-query
       @conn
       concept-pull-pattern))


(defn build-lookup-map-type-legacy-id [concepts]
  (reduce
   (fn [acc element]
     (assoc-in
      acc
      [(:concept/type (first element))
       (:concept.external-database.ams-taxonomy-67/id (first element))]
      (first element)
      ))
   {}
   concepts))


(defn pretty-spit
  [file-name collection]
  (spit (java.io.File. file-name)
        (with-out-str (pp/write collection :dispatch pp/code-dispatch))))

#_(defn write-legacy-lookup-to-file []
  (pretty-spit
   "resources/legacy-id-concept-lookup.edn"
   (build-lookup-map-type-legacy-id (get-legacy-concepts))))

(defn write-legacy-lookup-to-file []
    (nippy/freeze-to-file
     "resources/legacy-id-concept-lookup.nippy"
     (build-lookup-map-type-legacy-id (get-legacy-concepts))))
